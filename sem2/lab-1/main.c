// Advanced Programming in the UNIX Enviroment, 2005

/* Our own header, to be included before all standard system headers */

#ifndef  _APUE_H
#define  _APUE_H

#if defined(SOLARIS)
#define _XOPEN_SOURCE  500  /* Single UNIX Specification, Version 2  for Solaris 9 */
#define CMSG_LEN(x)  _CMSG_DATA_ALIGN(sizeof(struct cmsghdr)+(x))
#elif !defined(BSD)
#define _XOPEN_SOURCE  600  /* Single UNIX Specification, Version 3 */
#endif

#include <sys/types.h>    /* some systems still require this */
#include <sys/stat.h>
#include <sys/termios.h>  /* for winsize */
#ifndef TIOCGWINSZ
#include <sys/ioctl.h>
#endif
#include <stdio.h>    /* for convenience */
#include <stdlib.h>    /* for convenience */
#include <stddef.h>    /* for offsetof */
#include <string.h>    /* for convenience */
#include <unistd.h>    /* for convenience */
#include <signal.h>    /* for SIG_ERR */

#define  MAXLINE  4096      /* max line length */

/*
 * Default file access permissions for new files.
 */
#define  FILE_MODE  (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

/*
 * Default permissions for new directories.
 */
#define  DIR_MODE  (FILE_MODE | S_IXUSR | S_IXGRP | S_IXOTH)

typedef  void  Sigfunc(int);  /* for signal handlers */

#if  defined(SIG_IGN) && !defined(SIG_ERR)
#define  SIG_ERR  ((Sigfunc *)-1)
#endif

#define  min(a,b)  ((a) < (b) ? (a) : (b))
#define  max(a,b)  ((a) > (b) ? (a) : (b))

/*
 * Prototypes for our own functions.
 */
char  *path_alloc(int *);        /* {Prog pathalloc} */
long   open_max(void);        /* {Prog openmax} */
void   clr_fl(int, int);        /* {Prog setfl} */
void   set_fl(int, int);        /* {Prog setfl} */
void   pr_exit(int);          /* {Prog prexit} */
void   pr_mask(const char *);      /* {Prog prmask} */
Sigfunc  *signal_intr(int, Sigfunc *);  /* {Prog signal_intr_function} */

int     tty_cbreak(int);        /* {Prog raw} */
int     tty_raw(int);          /* {Prog raw} */
int     tty_reset(int);        /* {Prog raw} */
void   tty_atexit(void);        /* {Prog raw} */
#ifdef  ECHO  /* only if <termios.h> has been included */
struct termios  *tty_termios(void);    /* {Prog raw} */
#endif

void   sleep_us(unsigned int);      /* {Ex sleepus} */
ssize_t   readn(int, void *, size_t);    /* {Prog readn_writen} */
ssize_t   writen(int, const void *, size_t);  /* {Prog readn_writen} */
void   daemonize(const char *);      /* {Prog daemoninit} */

int     s_pipe(int *);          /* {Progs streams_spipe sock_spipe} */
int     recv_fd(int, ssize_t (*func)(int,
             const void *, size_t));/* {Progs recvfd_streams recvfd_sockets} */
int     send_fd(int, int);        /* {Progs sendfd_streams sendfd_sockets} */
int     send_err(int, int,
              const char *);    /* {Prog senderr} */
int     serv_listen(const char *);    /* {Progs servlisten_streams servlisten_sockets} */
int     serv_accept(int, uid_t *);    /* {Progs servaccept_streams servaccept_sockets} */
int     cli_conn(const char *);    /* {Progs cliconn_streams cliconn_sockets} */
int     buf_args(char *, int (*func)(int,
              char **));      /* {Prog bufargs} */

int     ptym_open(char *, int);  /* {Progs3 ptyopen_streams ptyopen_bsd ptyopen_linux} */
int     ptys_open(char *);      /* {Progs3 ptyopen_streams ptyopen_bsd ptyopen_linux} */
#ifdef  TIOCGWINSZ
pid_t   pty_fork(int *, char *, int, const struct termios *,
              const struct winsize *);    /* {Prog ptyfork} */
#endif

int    lock_reg(int, int, int, off_t, int, off_t); /* {Prog lockreg} */
#define  read_lock(fd, offset, whence, len) \
      lock_reg((fd), F_SETLK, F_RDLCK, (offset), (whence), (len))
#define  readw_lock(fd, offset, whence, len) \
      lock_reg((fd), F_SETLKW, F_RDLCK, (offset), (whence), (len))
#define  write_lock(fd, offset, whence, len) \
      lock_reg((fd), F_SETLK, F_WRLCK, (offset), (whence), (len))
#define  writew_lock(fd, offset, whence, len) \
      lock_reg((fd), F_SETLKW, F_WRLCK, (offset), (whence), (len))
#define  un_lock(fd, offset, whence, len) \
      lock_reg((fd), F_SETLK, F_UNLCK, (offset), (whence), (len))

pid_t  lock_test(int, int, off_t, int, off_t);    /* {Prog locktest} */

#define  is_read_lockable(fd, offset, whence, len) \
      (lock_test((fd), F_RDLCK, (offset), (whence), (len)) == 0)
#define  is_write_lockable(fd, offset, whence, len) \
      (lock_test((fd), F_WRLCK, (offset), (whence), (len)) == 0)

void  err_dump(const char *, ...);    /* {App misc_source} */
void  err_msg(const char *, ...);
void  err_quit(const char *, ...);
void  err_exit(int, const char *, ...);
void  err_ret(const char *, ...);
void  err_sys(const char *, ...);

void  log_msg(const char *, ...);      /* {App misc_source} */
void  log_open(const char *, int, int);
void  log_quit(const char *, ...);
void  log_ret(const char *, ...);
void  log_sys(const char *, ...);

void  TELL_WAIT(void);    /* parent/child from {Sec race_conditions} */
void  TELL_PARENT(pid_t);
void  TELL_CHILD(pid_t);
void  WAIT_PARENT(void);
void  WAIT_CHILD(void);

#endif  /* _APUE_H */

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <syslog.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/resource.h>

#define LOCKFILE_PATH "/var/run/daemon.pid"
#define LOCKMODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

sigset_t mask;

static void err_doit(int errnoflag, int error, const char *fmt, va_list ap)
{
    char  buf[MAXLINE];

    vsnprintf(buf, MAXLINE, fmt, ap);
    if (errnoflag)
        snprintf(buf+strlen(buf), MAXLINE-strlen(buf), ": %s",
        strerror(error));
    strcat(buf, "\n");
    fflush(stdout);    /* in case stdout and stderr are the same */
    fputs(buf, stderr);
    fflush(NULL);    /* flushes all stdio output streams */
}

void err_exit(int error, const char *fmt, ...)
{
    va_list    ap;

    va_start(ap, fmt);
    err_doit(1, error, fmt, ap);
    va_end(ap);
    exit(1);
}

void err_quit(const char *fmt, ...)
{
    va_list    ap;

    va_start(ap, fmt);
    err_doit(0, 0, fmt, ap);
    va_end(ap);
    exit(1);
}



int lockfile(int fd)
{
    struct flock fl = {
        .l_type = F_WRLCK,
        .l_whence = SEEK_SET,
        .l_start = 0,
        .l_len = 0
    };

    int fdescr = fcntl(fd, F_SETLK, &fl);
    return fdescr;
}

int already_running()
{
    int fd = open(LOCKFILE_PATH, O_RDWR | O_CREAT, LOCKMODE);
    if (fd < 0)
    {
        syslog(LOG_ERR, "Failed to open! %s: %s", LOCKFILE_PATH, strerror(errno));
        exit(1);
    }
    if (lockfile(fd) < 0)
    {
        if ((errno == EACCES) || (errno == EAGAIN))
        {
            close(fd);
            return 1;
        }
        syslog(LOG_ERR, "Failed to lock!");
        exit(1);
    }

    char buffer[16];

    ftruncate(fd, 0);
    sprintf(buffer, "%ld", (long int)getpid());
    write(fd, buffer, strlen(buffer) + 1);
    return 0;
}

void daemonize(const char *cmd)
{
    // 1. Сбросить маску режима создания файла.
    umask(0);

    // Получить максимально возможный номер дескриптора файла.
    struct rlimit rl;
    if (getrlimit(RLIMIT_NOFILE, &rl) < 0)
        perror("Failed to get max descr no.!");


    // 2. Стать лидером нового сеанса, чтобы утратить управляющий терминал.
    pid_t pid;
    if ((pid = fork()) < 0)
        perror("Failed to fork!");
    else if (pid != 0)
        exit(0);

    // 3. Вызвать setsid()
    setsid();

    // Обеспечить невозможность обретения управляющего терминала в будущем.
    struct sigaction sa = { .sa_handler = SIG_IGN, .sa_flags = 0 };
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) < 0)
        perror("Failed to ignore SIGHUP");
    

    //4. Назначить корневой каталог текущим рабочим каталогом, чтобы впоследствии можно было отмонтировать файловую систему.
    if (chdir("/") < 0)
        perror("Failed to change directory to /");

    // 5. Закрыть все открытые файловые дескрипторы.
    if (rl.rlim_max == RLIM_INFINITY)
        rl.rlim_max = 1024;
    for (int i = 0; i < rl.rlim_max; i++)
        close(i);

    // Присоединить файловые дескрипторы 0, 1 и 2 к /dev/null.
    int fd0 = open("/dev/null", O_RDWR);
    int fd1 = dup(0);
    int fd2 = dup(0);

    // 6. Инициализировать файл журнала.
    openlog(cmd, LOG_CONS, LOG_DAEMON);
    if (fd0 != 0 || fd1 != 1 || fd2 != 2)
    {
        syslog(LOG_ERR, "Unexpected file descriptors: %d %d %d",fd0, fd1, fd2);
        exit(1);
    }
}

void *thr_fn(void *arg)
{
    int err, signo;

    for (;;)
    {
        err = sigwait(&mask, &signo);
        if (err != 0) 
        {
            syslog(LOG_ERR, "Sigwait failed");
            exit(1);
        }

        switch (signo) 
        {
            case SIGHUP:
                syslog(LOG_WARNING, "<LOGGING> Re-reading configuration file");
                break;

            case SIGTERM:
                syslog(LOG_WARNING, "Got SIGTERM; exiting...");
                exit(0);

            default:
                syslog(LOG_WARNING, "unexpected signal %d\n", signo);
        }
  }

  return 0;
}

int main()
{
    int err;

    daemonize("DAEMON");
    if (already_running())
    {
        syslog(LOG_ERR, "My Daemon is already running!");
        exit(1);
    }
    
    struct sigaction sa = { .sa_handler = SIG_DFL };
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;

    if (sigaction(SIGHUP, &sa, NULL) < 0)
        err_quit("Failed to restore SIGHUP default");
    sigfillset(&mask);

    if ((err = pthread_sigmask(SIG_BLOCK, &mask, NULL)) != 0)
        err_exit(err, "SIG_BLOCK error");

    /*
    * Create a thread to handle SIGHUP and SIGTERM.
    */
    pthread_t tid;
    err = pthread_create(&tid, NULL, thr_fn, 0);
    if (err != 0)
        err_exit(err, "Failed to create thread");

    /*
    * Proceed with the rest of the daemon.
    */
    /* ... */

    syslog(LOG_WARNING, "Starting daemon...");
    while(1) 
    {
        long int time_d = time(NULL);
        syslog(LOG_WARNING, "My daemon ----- %s\n", ctime(&time_d));
        sleep(4);
    }

    exit(0);
}
