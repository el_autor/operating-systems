#include <linux/init.h> 
#include <linux/module.h> 
#include "md.h" 

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Vlad Krivozubov");

static int __init md_init(void) 
{ 
   printk("module md3 started!\n"); 
   printk("string exported from md1 : %s\n", md1_data); 
   printk("string returned md1_proc() : %s\n", md1_proc()); 
   return -1; 
} 

static void __exit md_exit(void) 
{ 
   printk("module md3 unloaded!\n"); 
} 

module_init(md_init);
module_exit(md_exit);