#include <stdio.h>
#include <unistd.h>
#define BUFFSIZE 0x1000

int main(int argc, char* argv[])
{
	char buf[BUFFSIZE];
	int len;
	FILE* f = fopen("/proc/19158/cmdline","r");
    
    len = fread(buf, 1, BUFFSIZE, f);
    buf[len-1] = 0;
    
    printf("cmdline:%s\n", buf);
    
	fclose(f);
	return 0;
}