#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>

//отступ для вывода на экран
#define INDENT 5

#define STACK_SIZE 1000

//откуда строить дерево
#define ROOT "/Users/vlad/Downloads/operating-systems/sem2/"

//начальный уровень в дереве
#define START_LEVEL 0

#define CURRENT_DIR "."
#define PARENT_DIR ".."

#define ERR_NUM_CHDIR 1
#define ERR_INFO_CHDIR "error call CHDIR"

#define ERR_INFO_CALL_LSTAT "error call lstat"

#define DIR_IS_NOT_ASSESS "directory is not accessible"

#define TRUE 1
#define FALSE 0

struct ElementTree {
    char* name;
    int level;
};

struct Stack {
    struct ElementTree elements[STACK_SIZE];
    int index;
};

void pop(struct Stack* stack) {
    if (stack->index != -1) {
        stack->index -= 1;
    }
}

void push(struct Stack* stack, struct ElementTree element) {
    stack->index += 1;
    stack->elements[stack->index] = element;
}

struct ElementTree top(struct Stack* stack) {
    return stack->elements[stack->index];
}

int isEmpty(struct Stack* stack) {
    if (stack->index == -1) {
        return TRUE;
    }

    return FALSE;
}

int main() {
    //уровень в дереве, на котором мы находимся на текущей итерации
    int current_level = START_LEVEL;
    if (chdir(ROOT) < 0) {
        printf(ERR_INFO_CHDIR);
        exit(ERR_NUM_CHDIR);
    }
    printf("\nStart program for directory:\n%s\n", ROOT);

    struct ElementTree head;
    head.name = (char *) CURRENT_DIR;
    head.level = 1;

    struct Stack stack;

    stack.index = -1;
    push(&stack, head);

    do {
        head = top(&stack);
        pop(&stack);

        //на (count_level_up + 1) надо подняться по дереву в сторону корня
        int count_level_up = current_level - head.level;
        for (int i = 0; i <= count_level_up; ++i) {
            if (chdir(PARENT_DIR) < 0) {
                printf(ERR_INFO_CHDIR);
                exit(ERR_NUM_CHDIR);
            }
        }

        current_level = head.level;
        if (chdir(head.name) < 0) {
            printf(ERR_INFO_CHDIR);
            exit(ERR_NUM_CHDIR);
        }

        DIR *dp;
        if ((dp = opendir(CURRENT_DIR)) == NULL ) {
            printf(DIR_IS_NOT_ASSESS);
            continue;
        }

        //печатаем информацию о текущей папке
        printf("%*s%s\n", head.level * INDENT, "-(D) ", head.name);

        struct dirent *dirp;
        while ((dirp = readdir(dp)) != NULL) {
            if (strcmp(dirp->d_name, CURRENT_DIR) == 0 || strcmp(dirp->d_name, PARENT_DIR) == 0) {
                continue;
            }

            struct stat statbuf;
            if (lstat(dirp->d_name, &statbuf) < 0) {
                printf(ERR_INFO_CALL_LSTAT);
                continue;
            }

            if (S_ISDIR(statbuf.st_mode) == 0) {
                //печатаем информацию о текущем файле
                printf("%*s%s\n", (head.level + 1) * INDENT, "-(F) ", dirp->d_name);
                continue;
            }

            //если до сюда дошла программа, то dirp - папка
            struct ElementTree new_el;
            new_el.name = (char *) dirp->d_name;
            new_el.level = head.level + 1;
            push(&stack, new_el);
        }
    } while(!isEmpty(&stack));

    return 0;
}