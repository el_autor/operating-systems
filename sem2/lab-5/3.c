#include <stdio.h>
#define FILENAME "os5_3_result.txt"

int main()
{
    FILE *fs1 = fopen(FILENAME, "w");
    FILE *fs2 = fopen(FILENAME, "w");
    
    for (char c = 'a'; c <= 'z'; c++)
    {
        if (c % 2)
            fprintf(fs1, "%c", c);
        else
            fprintf(fs2, "%c", c);
    }
    
    fclose(fs1);
    fclose(fs2);
    
    return 0;
}