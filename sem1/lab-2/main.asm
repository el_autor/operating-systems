.586P

descr struc

lim dw 0
base_l dw 0
base_m db 0
attr_1 db 0
attr_2 db 0
base_h db 0

descr ends



data segment use16

gdt_null descr <0, 0, 0, 0, 0, 0>
gdt_data descr <data_size - 1, 0, 0, 92h, 0, 0>
gdt_code descr <code_size - 1, 0, 0, 98h, 0, 0>
gdt_stack descr <255, 0, 0, 92h, 0, 0>
gdt_screen descr <3999, 8000h, 0Bh, 92h, 0, 0>
gdt_size = $-gdt_null

pdescr df 0
sym db 1
attr db 1Eh
msg_safe_first db 'Real mode!',0dh,0ah,0dh,0ah,'$'
msg_protected db 'Protected mode!', 0dh,0ah,'$'
msg_safe_last db 'Real mode!$'
data_size = $-gdt_null

data ends

text segment use16
     assume CS:text, DS:data

main proc
    xor EAX, EAX
    mov AX, data
    mov DS, AX

    shl EAX, 4
    mov EBP, EAX
    mov BX, offset gdt_data
    mov [BX].descr.base_l, AX
    shr EAX, 16
    mov [BX].descr.base_m, AL

    xor EAX, EAX
    mov AX, CS
    shl EAX, 4
    mov BX, offset gdt_code
    mov [BX].descr.base_l, AX
    shr EAX, 16
    mov [BX].descr.base_m, AL

    xor EAX, EAX
    mov AX, SS
    shl EAX, 4 
    mov BX, offset gdt_stack
    mov [BX].descr.base_l, AX
    shr EAX, 16
    mov [BX].descr.base_m, AL

    mov dword ptr pdescr+2, EBP
    mov word ptr pdescr, gdt_size - 1
    lgdt pdescr

    mov AX, 40h
    mov ES, AX
    mov word ptr ES:[67h], offset return
    mov AL, 0Fh
    mov ES:[69h], CS
    out 70h, AL
    mov AL, 0Ah
    out 71h, AL
    cli

    mov AH, 09h
    mov DX, offset msg_safe_first
    int 21h

    mov EAX, CR0
    or EAX, 1
    mov CR0, EAX

     ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    db 0EAh
    dw offset continue
    dw 16

continue:
    mov AX, 8
    mov DS, AX
    mov AX, 24
    mov SS, AX
    mov AX, 32
    mov ES, AX
    mov DI, 1920
    mov CX, 80
    mov AX, word ptr sym

    lea     BX, msg_protected
    xor     SI, SI
    mov     DI, 3680

next_symb:
    mov     AX, word ptr [BX + SI]
    mov     AH, 7
    stosw

    inc     SI
    cmp     SI, 15
    jne     next_symb

    ; mov AL, 0FEh
    ; out 64h, AL
    ; hlt

    mov gdt_data.lim, 0FFFFh
    mov gdt_code.lim, 0FFFFh
    mov gdt_screen.lim, 0FFFFh
    mov gdt_stack.lim, 0FFFFh
    push DS
    pop DS
    push SS
    pop SS
    push ES
    pop ES

    db 0EAh
    dw offset go
    dw 16

go: mov EAX, CR0
    and EAX, 0FFFFFFFEh
    mov CR0, EAX
    db 0EAh
    dw offset return
    dw text

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

return:
    mov AX, data
    mov DS, AX
    mov AX, stk
    mov SS, AX
    mov SP, 256
    sti

    mov AH, 09h
    mov DX, offset msg_safe_last
    int 21h
    mov AX, 4C00h
    int 21h

main endp
code_size = $-main
text ends


stk segment stack use16
    db 256 dup ('^')
stk ends


end main
     
