#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

const int totalProducers = 5;
const int totalConsumers = 5;  						
const int bufferSize = 10;			
const int count = 5;		
const int perm = S_IRWXU | S_IRWXG | S_IRWXO;	

char* sharedBuffer;
char* posToWrite;
char* posToRead;

#define SB 0
#define SE 1
#define SF 2

#define P -1
#define V 1

struct sembuf producerStart[2] = { {SE, P, 0}, {SB, P, 0} };
struct sembuf producerStop[2] =  { {SB, V, 0}, {SF, V, 0} };
struct sembuf consumerStart[2] = { {SF, P, 0}, {SB, P, 0} };
struct sembuf consumerStop[2] =  { {SB, V, 0}, {SE, V, 0} };

void producer(const int semid, const int consumerNum, char symb)
{
	sleep(rand() % 4);

	int semP = semop(semid, producerStart, 2);
	if (semP == -1)
	{
		perror("Can't make operation on semaphors.");
		exit(1);
	}

	sharedBuffer[(*posToWrite) % bufferSize] = symb;
	printf("Producer #%d ----> %c\n", consumerNum, sharedBuffer[(*posToWrite) % bufferSize]);
	(*posToWrite)++;

	int semV = semop(semid, producerStop, 2);
	if (semV == -1)
	{
		perror("Can't make operation on semaphors.");
		exit(1);
	}
}

void consumer(const int semid, const int consumerNum)
{
	sleep(rand() % 3);

	int semP = semop(semid, consumerStart, 2);
	if (semP == -1)
	{
		perror("Can't make operation on semaphors.");
		exit(1);
	}

	printf("Consumer #%d <---- %c\n", consumerNum, sharedBuffer[(*posToRead) % bufferSize]);
	(*posToRead)++;

	int semV = semop(semid, consumerStop, 2);
	if (semV == -1)
	{
		perror("Can't make operation on semaphors.");
		exit(1);
	}
}

int main()
{
	int shmid, semid; 

	int parent_pid = getpid();
  	printf("Parent pid: %d\n", parent_pid);

	if ((shmid = shmget(IPC_PRIVATE, (bufferSize + 2) * sizeof(char), IPC_CREAT | perm)) == -1) 
	{
		perror("Unable to create a shared area.");
		exit(1);
	}

	sharedBuffer = shmat(shmid, 0, 0); 
	if (*sharedBuffer == -1)
	{
		perror("Can't attach memory");
		exit(1);
	}

    posToWrite = sharedBuffer;
	posToRead = sharedBuffer + sizeof(char);
    sharedBuffer = sharedBuffer + sizeof(char) * 2;

	*posToWrite = 0;
    *posToRead = 0;

	if ((semid = semget(IPC_PRIVATE, 3, IPC_CREAT | perm)) == -1) 
	{
		perror("Unable to create a semaphore.");
		exit( 1 );
	}

	int ret_sb = semctl(semid, SB, SETVAL, 1);
	int ret_se = semctl(semid, SE, SETVAL, bufferSize); 
	int ret_sf = semctl(semid, SF, SETVAL, 0);

	if ( ret_se == -1 || ret_sf == -1 || ret_sb == -1)
	{
		perror("Can't set control semaphors.");
		exit(1);
	}

	pid_t pid;
	for (size_t i = 0; i < totalProducers; i++)
	{
		switch (pid = fork())
		{
		case -1:
			printf("Can't fork.");
			exit(1);
		case 0:
			for (size_t j = 0; j < count; j++) {
				producer(semid, i + 1, (char)('A' + i));	
			}
			exit(0);
		}
	}


    for (size_t i = 0; i < totalConsumers; i++)
	{
		switch (pid = fork())
		{
		case -1:
			printf("Can't fork.");
			exit(1);
		case 0:
			for (size_t j = 0; j < count; j++) {
				consumer(semid, i + 1);
			}
			exit(0);
		}
	}

	for (size_t i = 0; i < totalConsumers + totalProducers; i++)
	{
		int status;
		pid_t ret_value = wait(&status);

		if (WIFEXITED(status))
		    printf("Child %d finished with %d code.\n\n", ret_value, WEXITSTATUS(status));
		else if (WIFSIGNALED(status))
		    printf("Child %d finished from signal with %d code.\n\n", ret_value, WTERMSIG(status));
		else if (WIFSTOPPED(status))
			printf("Child %d finished from signal with %d code.\n\n", ret_value, WSTOPSIG(status));
	}

    printf("%d\n", *posToWrite);
    if (shmdt(posToWrite) == -1) 
	{
		perror("Can't detach shared memory");
		exit(1);
	}

    exit(0);
}