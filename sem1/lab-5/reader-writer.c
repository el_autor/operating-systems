#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

#define TOTAL_WRITERS 5
#define TOTAL_READERS 5

#define ACTIVE_READER 0
#define ACTIVE_WRITER 1
#define BIN_ACTIVE_WRITER 2
#define WAITING_WRITER 3

struct sembuf startRead[] = { { WAITING_WRITER, 0, 0 },
                              { ACTIVE_WRITER,  0, 0 },
                              { ACTIVE_READER,  1, 0 } };

struct sembuf  stopRead[] = { {ACTIVE_READER, -1, 0} };

struct sembuf  startWrite[] = { { WAITING_WRITER,     1, 0 }, 
                                { ACTIVE_READER,      0, 0 }, 
                                { BIN_ACTIVE_WRITER, -1, 0 }, 
                                { ACTIVE_WRITER,      1, 0 }, 
                                { WAITING_WRITER,    -1, 0 } };

struct sembuf  stopWrite[] = { { ACTIVE_WRITER,    -1, 0 }, 
                               { BIN_ACTIVE_WRITER, 1, 0 }};

const int perm =  S_IRWXU | S_IRWXG | S_IRWXO;

void writer(int semid, int* buffer, int num) {
    while (1)
    {
        semop(semid, startWrite, 5);
        (*buffer)++;
		printf("Writer #%d ----> %d\n", num, *buffer);
        semop(semid, stopWrite, 2);
		sleep(rand() % 4);
    }
}

void reader(int semid, int* buffer, int num) {
    while (1) 
    {
        semop(semid, startRead, 3);
		printf("Reader #%d <---- %d\n", num, *buffer);
        semop(semid, stopRead, 1);
		sleep(rand() % 2);
    }
}

int main() 
{
    srand(time(NULL));

    int parent_pid = getpid();
  	printf("Parent pid: %d\n", parent_pid);

    int shm_id;
    if ((shm_id = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | perm)) == -1) 
	{
		perror("Unable to create a shared area.");
		exit(1);
	}

    int *sharedBuffer = shmat(shm_id, 0, 0); 
    if (sharedBuffer == (void*) -1) 
    {
        perror("Can't attach memory");
        exit(1);
    }

    (*sharedBuffer) = 0;

    int sem_id;
    if ((sem_id = semget(IPC_PRIVATE, 4, IPC_CREAT | perm)) == -1) 
	{
		perror("Unable to create a semaphore.");
		exit(1);
	}

	int retValue = semctl(sem_id, BIN_ACTIVE_WRITER, SETVAL, 1);
    if (retValue == -1)
	{
		perror( "Can't set control semaphors.");
		exit(1);
	}

	pid_t pid = -1;

	for (int i = 0; i < TOTAL_WRITERS && pid != 0; i++) {
        pid = fork();
        if (pid == -1) {
            perror("Writer's fork error.");
            exit(1);
        }
        if (pid == 0) {
            writer(sem_id, sharedBuffer, i);
        }
	}

    for (int i = 0; i < TOTAL_READERS && pid != 0; i++) {
        pid = fork();
        if (pid == -1) {
            perror("Reader's fork error."); 
            exit(1);
        }
        if (pid == 0) {
            reader(sem_id, sharedBuffer, i);
        }
	}

    if (shmdt(sharedBuffer) == -1) {
        perror("Can't detach shared memory");
        exit(1);
    }

    if (pid != 0) {
        int *status;

        for (int i = 0; i < TOTAL_READERS + TOTAL_WRITERS; ++i) {
	        wait(status);
        }

        if (shmctl(shm_id, IPC_RMID, NULL) == -1) {
            perror("Can't free memory!");
            exit(1);
        }
    }

    exit(0);
}