#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() 
{
    int child1 = fork();
    //printf("%d\n", child1);

    if (child1 == -1) 
    {
        printf("Cant fork");
        exit(1);
    }
    else if (child1 == 0)
    {
        // Код потомков
        printf("Child1: pid=%d;	group=%d;	parent=%d\n", getpid(), getpgrp(), getppid());
        sleep(1);
        printf("Child1: pid=%d;	group=%d;	parent=%d\n", getpid(), getpgrp(), getppid());

        return 0;
    }
    else 
    {
        // Код предка
        printf( "Parent: pid=%d;	group=%d;	child1=%d\n", getpid(), getpgrp(), child1);
    }

    int child2 = fork();
    //printf("%d\n", child2);

    if (child2 == -1) 
    {
        printf("Cant fork");
        exit(1);
    }
    else if (child2 == 0)
    {
        // Код потомков
        sleep(1);
        printf("Child2: pid=%d;	group=%d;	parent=%d\n", getpid(), getpgrp(), getppid());

        return 0;
    }
    else 
    {
        // Код предка
        printf( "Parent: pid=%d;	group=%d;	child2=%d\n", getpid(), getpgrp(), child2);

        return 0;
    }
}