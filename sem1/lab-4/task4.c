#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
	int descr[2]; //дескриптор _одного_ программного канала
	//[0] - выход для чтения, [1] - выход для записи
	//потомок унаследует открытый программный канал предка

	if (pipe(descr) == -1)
	{
        printf("Couldn't pipe.");
		exit(1);
	}

    int child1 = fork();
    if ( child1 == -1 )
	{
        printf("Couldn't fork.");
		exit(1);
	}
    if (child1 == 0)
	{
		// код потомка
        close(descr[0]); // потомок ничего не читает из канала

        char msg1[] = "msg1";

		// пишем в канал
        write(descr[1], msg1, 16);

        exit(0);
	}

    int child2 = fork();
    if (child2 == -1)
    {
        printf( "Couldn't fork." );
        exit(1);
    }
    if (child2 == 0)
    {
        // код потомка
        close(descr[0]); //потомок ничего не читает из канала

        char msg2[] = "msg2";

        // пишем в канал
        write(descr[1], msg2, 16);

        exit(0);
	}
	
	if (child1 != 0 && child2 != 0)
	{
		// код предка
		close(descr[1]); //предок ничего не пишет в канал
		char msg1[16];
		read(descr[0], msg1, 16); // читаем из канала

		char msg2[16];
		read(descr[0], msg2, 16); // читаем из канала

		printf("Parent: reads %s %s\n\n", msg1, msg2);

		int status;
		pid_t ret_value;
		ret_value = wait(&status);
		if (WIFEXITED(status))
		    printf("Parent: child %d finished with %d code.\n\n", ret_value, WEXITSTATUS(status));
		else if (WIFSIGNALED(status))
		    printf("Parent: child %d finished from signal with %d code.\n\n", ret_value, WTERMSIG(status));
		else if (WIFSTOPPED(status))
			printf("Parent: child %d finished from signal with %d code.\n\n", ret_value, WSTOPSIG(status));
			
		ret_value = wait(&status);
		if (WIFEXITED(status))
			printf("Parent: child %d finished with %d code.\n\n", ret_value, WEXITSTATUS(status));
		else if (WIFSIGNALED(status))
			printf("Parent: child %d finished from signal with %d code.\n\n", ret_value, WTERMSIG(status));
		else if (WIFSTOPPED(status))
			printf("Parent: child %d finished from signal with %d code.\n\n", ret_value, WSTOPSIG(status));

    }

    return 0;
}