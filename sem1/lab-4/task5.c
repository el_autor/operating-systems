#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>

#define BUFSIZE 100
#define WAITING 5
#define CHILD_NUM 2

int pid;
int childPids[CHILD_NUM];
char messages[CHILD_NUM][16] = { "Hello1", "Hello2" };
int state = 0;

void ignoreSignal(int sigNum) {}

void isWriting(int sigNum)
{
    state = 1;
}

int main()
{
	int descr[2]; 
	char buffer[BUFSIZE] = "";

	printf("Parent process: PID=%d, GROUP=%d\n", getpid(), getpgrp());

	if (pipe(descr) == -1)
    {
        printf("Couldn't pipe.");
		exit(1);
	}

	signal(SIGINT, ignoreSignal);

	for (size_t i = 0; i < CHILD_NUM; i++)
	{
		switch (pid = fork())
		{
		case -1:
			printf("Can't fork.");
			exit(1);
		case 0:
			signal(SIGINT, isWriting);
			sleep(WAITING);

			if (state)
			{
				close(descr[0]);
				write(descr[1], messages[i], strlen(messages[i]));
				printf("Message was sent to parent.\n");
			}
			else 
			{
				printf("No signal sent.\n");
			}		

			exit(0);
		default:
			childPids[i] = pid;
		}
	}

	for (size_t i = 0; i < CHILD_NUM; i++)
	{
		int status;
		pid_t ret_value = wait(&status);

		printf("Child process has finished: PID = %d, status = %d\n", ret_value, status);

		if (WIFEXITED(status))
		    printf("Child %d finished with %d code.\n\n", ret_value, WEXITSTATUS(status));
		else if (WIFSIGNALED(status))
		    printf("Child %d finished from signal with %d code.\n\n", ret_value, WTERMSIG(status));
		else if (WIFSTOPPED(status))
			printf("Child %d finished from signal with %d code.\n\n", ret_value, WSTOPSIG(status));
	}
	
	close(descr[1]);
	read(descr[0], buffer, BUFSIZE);

	printf("Received message: %s\n", buffer);
	printf("Parent process have children with IDs: %d, %d\n", childPids[0], childPids[1]);
    printf("Parent process is dead now\n");

	return 0;
}
